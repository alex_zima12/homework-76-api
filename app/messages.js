const express = require('express');
const {nanoid} = require('nanoid');
const router = express.Router();
const fs = require('fs');
const path = "./messages/db.json";
let dataList = [];


router.get('/', (req, res) => {

    dataList = JSON.parse(fs.readFileSync(path));
    if (req.query.datetime) {
        const date = new Date(req.query.datetime);
        if (isNaN(date.getDate())) {
            return res.status(400).send({error: "Invalid date"})
        }
        const result = dataList.filter(message => {
            console.log(req.query.datetime)
            return req.query.datetime < message.date
        })
        res.send(result)
         } else if (dataList.length > 30){
        let newDataList = dataList.splice(dataList.length - 30 , dataList.length - 1);
        res.send(newDataList);
    } else {
        res.send(dataList);
    }

    });

router.post('/', (req, res) => {
    const date = new Date().toISOString();
    const id = nanoid();
    const messages = {...req.body, date, id};
    if (!req.body.author || !req.body.message) {
        return res.status(404).send({error: "Author and message must be present in the request"})
    }
    dataList.push(messages)
    fs.writeFile(path, JSON.stringify(dataList), (err) => {
        if (err) {
            console.error(err);
        }
        console.log('File was saved!');
    });
    res.send('File was saved!');
});

module.exports = router;